const config = {
  langs: {
    es: 'es-ES',
    en: 'en-GB',
  },
  pages: {
    index: 'index',
    jobs: 'jobs',
    services: 'services',
    mentoring: 'mentoring',
    merliners: 'merliners',
    blog: 'blog',
    post: 'post'
  },
}

exports.defaults = config
