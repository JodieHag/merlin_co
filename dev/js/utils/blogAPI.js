import { getLang } from './index'

export const cleanAllPosts = (parent) => {
  if (parent?.firstChild) {
    while (parent.firstChild) {
      parent.removeChild(parent.lastChild)
    }
  }
  return true
}

export const createCard = ({
  title,
  description,
  copyButton,
  sourceImage,
  altImage,
  category,
  id,
  slug,
  dataPost,
}) => {
  let card = document.createElement('div')
  card.classList.add('card')
  let cardShadow = document.createElement('div')
  cardShadow.classList.add('card__shadow')
  let cardChild = document.createElement('div')
  cardChild.classList.add('card__child')
  cardChild.classList.add('card__child--blog')
  let cardImage = document.createElement('div')
  cardImage.classList.add('card-img')
  let cardPicture = document.createElement('picture')
  let cardSource = document.createElement('source')
  let cardImg = document.createElement('img')
  let cardTitle = document.createElement('div')
  cardTitle.classList.add('card-title')
  let cardInfo = document.createElement('div')
  cardInfo.classList.add('card-info')
  let cardDepartment = document.createElement('div')
  cardDepartment.classList.add('card-department')
  let cardCategory = document.createElement('span')
  cardDepartment.classList.add('card-category')
  let cardDate = document.createElement('span')
  cardDepartment.classList.add('card-date')
  let cardDescription = document.createElement('div')
  cardDescription.classList.add('card-description')
  let cardAction = document.createElement('div')
  cardAction.classList.add('card-action')
  let cardActionButton = document.createElement('a')
  cardActionButton.classList.add('mm-button-secondary--clean')

  card.appendChild(cardShadow)
  card.appendChild(cardChild)
  cardChild.appendChild(cardImage)
  cardImage.appendChild(cardPicture)
  cardPicture.appendChild(cardSource)
  cardPicture.appendChild(cardImg)
  cardChild.appendChild(cardTitle)
  cardChild.appendChild(cardInfo)
  cardInfo.appendChild(cardDepartment)
  cardDepartment.appendChild(cardCategory)
  cardDepartment.appendChild(cardDate)
  cardInfo.appendChild(cardDescription)
  cardInfo.appendChild(cardAction)
  cardAction.appendChild(cardActionButton)

  cardTitle.innerHTML = title
  cardDescription.innerHTML = description
  cardActionButton.innerHTML = copyButton
  cardSource.src = sourceImage
  cardSource.srcset = sourceImage
  cardImg.src = sourceImage
  cardImg.alt = altImage
  cardActionButton.id = id
  cardActionButton.value = slug
  cardCategory.innerHTML = `${category} | `
  cardActionButton.addEventListener('click', (e) => {
    e.preventDefault()
    e.stopPropagation()
    window.open(
      `/${getLang()}/post.html?slug=${e.target.value}&id=${e.target.id}`,
      '_self'
    )
  })
  const date = new Date(dataPost)
  cardDate.innerHTML = `${date.getDate()}/${
    date.getMonth() + 1
  }/${date.getFullYear()}`
  return card
}
export const getPostsByCategory = (categoryID, parent, excludedIDPost) => {
  fetch(
    `https://blog.merlindp.com/wp-json/wp/v2/posts?status=publish&_embed${
      categoryID === 'all' ? '' : `&categories=${categoryID}`
    }`,
    {
      method: 'GET',
    }
  )
    .then((res) => res.json())
    .then((res) => {
      let data = res
      cleanAllPosts(parent)
      if (excludedIDPost) {
        data = data.filter((element) => element.id !== Number(excludedIDPost))
      }
      const emptySelector = document.querySelector('.empty-js')
      if (data?.length && emptySelector) {
        emptySelector.remove()
      }
      const maxCards = excludedIDPost
        ? data.length <= 3
          ? data.length
          : 3
        : data.length
      for (let i = 0; i < maxCards; i++) {
        let card = createCard({
          title: data[i]?.title?.rendered ?? '',
          description: data[i]?.content?.rendered ?? '',
          copyButton: `${getLang() === 'es' ? 'Seguir leyendo' : 'Read more'}`,
          sourceImage:
            data[i]?._embedded?.['wp:featuredmedia']?.[0]?.source_url,
          altImage: data[i]?._embedded?.['wp:featuredmedia']?.[0]?.alt_text,
          category: data[i]?._embedded?.['wp:term']?.[0]?.[0]?.name,
          dataPost: data[i]?.modified,
          id: data[i]?.id,
          slug: data[i]?.slug,
        })
        parent.appendChild(card)
      }
    })
}

export const getLastPosts = (parent, maxLength) => {
  fetch('https://blog.merlindp.com/wp-json/wp/v2/posts?status=publish&_embed', {
    method: 'GET',
  })
    .then((res) => res.json())
    .then((res) => {
      const data = res
      const maxCounter = maxLength
        ? data.length <= maxLength
          ? data.length
          : maxLength
        : data.length
      for (let i = 0; i < maxCounter; i += 1) {
        let card = createCard({
          title: data[i]?.title?.rendered ?? '',
          description: data[i]?.content?.rendered ?? '',
          copyButton: `${getLang() === 'es' ? 'Seguir leyendo' : 'Read more'}`,
          sourceImage:
            data[i]?._embedded?.['wp:featuredmedia']?.[0]?.source_url,
          altImage: data[i]?._embedded?.['wp:featuredmedia']?.[0]?.alt_text,
          category: data[i]?._embedded?.['wp:term']?.[0]?.[0]?.name,
          dataPost: data[i]?.modified,
          id: data[i]?.id,
          slug: data[i]?.slug,
        })
        parent.appendChild(card)
      }
    })
}
