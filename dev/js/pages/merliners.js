import '../main'

const splitPhotos = document.querySelectorAll('.default')

if (splitPhotos) {
  splitPhotos.forEach((photo) => {
    photo.querySelector('img').addEventListener('mouseenter', () => {
      photo.classList.add('mm-not-opacity')
    })
    photo.querySelector('img').addEventListener('mouseleave', () => {
      photo.classList.remove('mm-not-opacity')
    })
  })
}
