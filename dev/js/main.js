import './molecules/menu'
import './molecules/top'
import './molecules/forms'
import AOS from 'aos' // You can also use <link> for styles
// ..

const langMenu = document.querySelector('.menu-lang')
if (
  /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
    navigator.userAgent
  )
) {
  // true for mobile device
  const scrollFunction = () => {
    if (
      document.body.scrollTop > 20 ||
      document.documentElement.scrollTop > 20
    ) {
      langMenu.style.display = 'none'
    } else {
      langMenu.style.display = 'block'
    }
  }

  window.onscroll = () => {
    scrollFunction()
  }
} else {
  // false for not mobile device
  const styleElement = document.createElement('link')
  styleElement.href = 'https://unpkg.com/aos@next/dist/aos.css'
  styleElement.rel = 'stylesheet'
  document.getElementsByTagName('head')[0].appendChild(styleElement)
  AOS.init()
}

const setCookie = (name, value, box) => {
  const d = new Date()
  d.setTime(d.getTime() + 10 * 24 * 60 * 60 * 10000)
  const expires = 'expires=' + d.toUTCString()
  const path = 'path=/'

  document.cookie = `${name}=${value};${expires};${path}`

  // remove box
  if (box) {
    box.style.display = 'none'
  }
}

const getCookie = (cname) => {
  var name = cname + '='
  var ca = document.cookie.split(';')
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i]
    while (c.charAt(0) == ' ') {
      c = c.substring(1)
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length)
    }
  }
  return ''
}

const cookie = getCookie('merlin_cook')

const cookiesDiv = document.querySelector('.cookies')
if (cookie) {
  cookiesDiv.remove()
} else {
  if (cookiesDiv) {
    document.querySelector('.js-yes-cookie').addEventListener('click', (e) => {
      e.preventDefault()
      setCookie('merlin_cook', 'yes', cookiesDiv)
      dataLayer.push({ event: 'cookiepermission first page yes' })
      window.location.reload()
    })
    document.querySelector('.js-no-cookie').addEventListener('click', (e) => {
      e.preventDefault()
      setCookie('merlin_cook', 'no', cookiesDiv)
      dataLayer.push({ event: 'cookiepermission first page no' })
    })
  }
}

console.log(
  'Powered By: JellyBrains Digital Studio S.L.',
  'https://www.jelly-brains.com'
)
