import { getLastPosts, getPostsByCategory } from '../utils/blogAPI'

const blogWrapper = document.querySelector('.list-post')
getLastPosts(blogWrapper)

const getCategories = () => {
  const selector = document.querySelector('.filter-categories')
  const listBlog = document.querySelector('.list-post')
  selector.addEventListener('change', (e) => {
    getPostsByCategory(e.target.value, listBlog)
  })
  fetch(
    'https://blog.merlindp.com/wp-json/wp/v2/categories?orderby=name&hide_empty=true',
    {
      method: 'GET',
    }
  )
    .then((res) => res.json())
    .then((res) => {
      for (let i = 0; i < res.length; i++) {
        let opt = document.createElement('option')
        opt.value = res[i].id
        opt.id = res[i].id
        opt.innerHTML = res[i].name
        selector.appendChild(opt)
      }
    })
}

// Fill Select Categories
getCategories()
