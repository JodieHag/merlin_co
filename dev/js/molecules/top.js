const topButton = document.querySelector('.js-top')
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = () => {
  scrollFunction()
}

const scrollFunction = () => {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    topButton.style.display = 'block'
  } else {
    topButton.style.display = 'none'
  }
}

topButton.addEventListener('click', () => {
  document.documentElement.scrollTop = 0
  document.body.scrollTop = 0
})
