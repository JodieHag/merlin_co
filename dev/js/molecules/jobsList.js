import { getLang } from '../utils'

const GET_JOBS_URL = 'https://api.teamtailor.com/v1/jobs'
const PER_PAGE = 15
const FEED = 'public'
const JOBS_PARAMETERS = `?&page[size]=${PER_PAGE}&filter[feed]=${FEED}`
const API_TOKEN = 'Token token=Gbr2JeEkJKcdtbqmyj63gb0EQbcisRc_Byz9gKxW'

const getJobsFromTeamTailor = (url) => {
  let listWrapper = document.querySelector('.jobs-list ')
  fetch(url || `${GET_JOBS_URL}${JOBS_PARAMETERS}`, {
    method: 'GET',
    headers: {
      Authorization: API_TOKEN,
      'X-Api-Version': '20210218',
    },
  })
    .then((res) => res.json())
    .then((res) => {
      const jobWrapper = document.querySelector('.teamtailor-jobs__job-wrapper')
      const { data } = res

      for (let item of jobWrapper?.children) {
        let childContainer = document.createElement('div')
        childContainer.classList.add('teamtailor-jobs__child')
        childContainer.append(item.querySelector('.teamtailor-jobs__job-title'))
        childContainer.append(item.querySelector('.teamtailor-jobs__job-info'))
        if (!item.classList.contains('teamtailor-jobs__pagination')) {
          item.append(childContainer)
        }
        const title = item.querySelector('.teamtailor-jobs__job-title')
        const info = item.querySelector('.teamtailor-jobs__job-info')
        const elementMatch = data?.filter((element) => {
          return element?.attributes?.title === title?.innerHTML
        })
        let salary = ''
        let newTag = ''
        for (let match of elementMatch) {
          if (!item.querySelector('.teamtailor-jobs__job-new')) {
            if (match?.attributes['updated-at']) {
              const today = new Date()
              const updatedDay = new Date(match?.attributes['updated-at'])
              if (today.getMonth() === updatedDay.getMonth()) {
                newTag = `<span>${getLang() === 'es' ? 'Nueva' : 'New'}</span>`
              }
            }
            if (match?.attributes['min-salary']) {
              salary += `
              <span class='teamtailor-jobs__salary-min'>${match?.attributes['min-salary']}K</span>
            `
            }
            if (
              match?.attributes['min-salary'] &&
              match?.attributes['max-salary']
            ) {
              salary += `
               -
            `
            }
            if (match?.attributes['max-salary']) {
              salary += `
              <span class='teamtailor-jobs__salary-max'>${match?.attributes['max-salary']}K</span>
            `
            }
          }
        }

        let newTagContainer = document.createElement('div')
        newTagContainer.classList.add('teamtailor-jobs__job-new')
        if (newTag) {
          newTagContainer.innerHTML = newTag
          title.parentElement.insertBefore(
            newTagContainer,
            title.parentElement.firstChild
          )
        }

        let salaryWrapper = document.createElement('div')
        salaryWrapper.classList.add('teamtailor-jobs__salary')
        if (salary) {
          salaryWrapper.innerHTML = salary
          info.append(salaryWrapper)
        }

        if (!item.classList.contains('teamtailor-jobs__pagination')) {
          let shadowWrapper = document.createElement('div')
          shadowWrapper.classList.add('teamtailor-jobs__shadow')
          childContainer.parentElement.insertBefore(
            shadowWrapper,
            childContainer.parentElement.firstChild
          )

          let buttonWrapper = document.createElement('a')
          buttonWrapper.classList.add('teamtailor-jobs__action')
          buttonWrapper.href = title.href
          buttonWrapper.target = '_blank'
          buttonWrapper.innerHTML = `${
            getLang() === 'es' ? 'Ver oferta' : 'View offer'
          }`
          childContainer.append(buttonWrapper)
        }
      }

      const nextButton = document.querySelector(
        '.teamtailor-jobs__pagination__next'
      )
      const prevButton = document.querySelector(
        '.teamtailor-jobs__pagination__prev'
      )

      nextButton?.addEventListener('click', (event) => {
        listWrapper?.classList.add('mm-not-opacity')
        setTimeout(() => getJobsFromTeamTailor(nextButton?.href), 1000)
        document.querySelector('#jobs').scrollIntoView()
      })
      prevButton?.addEventListener('click', (event) => {
        listWrapper?.classList.add('mm-not-opacity')
        setTimeout(() => getJobsFromTeamTailor(prevButton?.href), 1000)
        document.querySelector('#jobs').scrollIntoView()
      })
      jobWrapper.classList.remove('mm-not-opacity')
      listWrapper.classList.remove('mm-not-opacity')
    })
    .catch((error) => console.log(error))
}

setTimeout(() => {
  getJobsFromTeamTailor()

  setTimeout(() => {
    const jobWrapper = document.querySelector('.teamtailor-jobs__job-wrapper')
    const filters = document.querySelectorAll('.teamtailor-jobs__select')
    if (filters?.length) {
      filters.forEach((filter, index) => {
        filter.id = index === 0 ? 'department' : 'location'
        if (index === 0) {
          // filter department
          filter.addEventListener('change', (element) => {
            jobWrapper.classList.add('mm-not-opacity')
            const valueDepartment = document.querySelector('#department').value
            const valueLocation = document.querySelector('#location').value
            setTimeout(() => {
              getJobsFromTeamTailor(
                `${GET_JOBS_URL}${JOBS_PARAMETERS}${
                  valueDepartment
                    ? `&filter[department]=${valueDepartment}`
                    : ''
                }${valueLocation ? `&filter[locations]=${valueLocation}` : ''}`
              )
            }, 300)
          })
        }
        if (index === 1) {
          // filter location
          filter.addEventListener('change', (element) => {
            jobWrapper.classList.add('mm-not-opacity')
            const valueDepartment = document.querySelector('#department').value
            const valueLocation = document.querySelector('#location').value
            setTimeout(() => {
              getJobsFromTeamTailor(
                `${GET_JOBS_URL}${JOBS_PARAMETERS}${
                  valueDepartment
                    ? `&filter[department]=${valueDepartment}`
                    : ''
                }${valueLocation ? `&filter[locations]=${valueLocation}` : ''}`
              )
            }, 300)
          })
        }
      })
    }
  }, 1400)
}, 700)
