// Partner form
import { getLang } from '../utils'
const locale = getLang()

hbspt.forms.create({
  region: 'eu1',
  portalId: '25824769',
  formId:
    locale === 'en'
      ? 'b7079323-ac09-4792-b0bf-d0caa3f202a0'
      : '6d3b2a95-ee99-4a1a-b659-ac9b562fca70',
  onFormSubmit: (form) => {},
})

// Contact form
hbspt.forms.create({
  region: 'eu1',
  portalId: '25824769',
  formId:
    locale === 'en'
      ? '8c659a0c-b529-43cd-845e-9c3741fd1795'
      : 'a3ceaf18-cb45-4e29-adb6-ab5f5a905407',
  onFormSubmit: (form) => {},
})

const wrapperModalPartner = document.querySelector('#forms')
const forms = document.querySelectorAll('.hbspt-form')

setTimeout(() => {
  forms.forEach((form) => {
    form.classList.add('mm-form-hide')
    wrapperModalPartner.appendChild(form)
    // form.remove()
  })

  const contactFormItems = document.querySelectorAll('.contact-form-action')
  const partnerFormItems = document.querySelectorAll('.partner-form-action')
  const closeIcon = document.querySelector('.mm-modal--close')

  closeIcon?.addEventListener('click', onHideModal)

  if (partnerFormItems) {
    partnerFormItems.forEach((el) => {
      el.addEventListener('click', onShowPartnerForm)
    })
  }
  if (contactFormItems) {
    contactFormItems.forEach((el) => {
      el.addEventListener('click', onShowContactForm)
    })
  }
}, 1000)

const onHideModal = (e) => {
  e.preventDefault()
  const modal = document.querySelector('.modal')
  modal.classList.add('mm-modal-hide')
  modal.classList.remove('mm-modal-show')
  forms.forEach((el) => {
    el.classList.add('mm-form-hide')
  })
  document.body.style.overflow = 'auto'
}

const onShowPartnerForm = (e) => {
  e.preventDefault()
  const modal = document.querySelector('.modal')
  modal.classList.remove('mm-modal-hide')
  modal.classList.add('mm-modal-show')
  forms[0].classList.remove('mm-form-hide')
  document.body.style.overflow = 'hidden'
}

const onShowContactForm = (e) => {
  e.preventDefault()
  const modal = document.querySelector('.modal')
  modal.classList.remove('mm-modal-hide')
  modal.classList.add('mm-modal-show')
  forms[1].classList.remove('mm-form-hide')
  document.body.style.overflow = 'hidden'
}
