import { getPostsByCategory } from '../utils/blogAPI'
import { getLang } from '../utils'

const getPostByID = () => {
  const queryString = window.location.search
  const urlParams = new URLSearchParams(queryString)
  const id = urlParams.get('id')
  if (!id) {
    window.open(`/${getLang()}/blog.html`, '_self')
  }
  fetch(`https://blog.merlindp.com/wp-json/wp/v2/posts/${id}?_embed`, {
    method: 'GET',
  })
    .then((res) => res.json())
    .then((res) => {
      const data = res
      document.title = `${data.title?.rendered} - Merlin Digital partner`

      const imageDetail = document.querySelector('.post-image')
      imageDetail.style.backgroundImage = `url('${data._embedded?.['wp:featuredmedia']?.[0]?.media_details?.sizes?.full?.source_url}')`
      const titleDetail = document.querySelector('.post-title')
      titleDetail.innerHTML = data.title?.rendered ?? ''
      const dateDetail = document.querySelector('.post-date')
      const date = new Date(data.modified)
      dateDetail.innerHTML = `${date.getDate()}/${
        date.getMonth() + 1
      }/${date.getFullYear()}`
      const categoryDetail = document.querySelector('.post-category')
      categoryDetail.innerHTML = data._embedded?.['wp:term']?.[0]?.[0]?.name
      const contentDetail = document.querySelector('.post-main')
      contentDetail.innerHTML = data.content.rendered
      const contentRecommend = document.querySelector('.list-recommend')

      // Get Recommended Posts by Category
      getPostsByCategory(
        data._embedded?.['wp:term']?.[0]?.[0]?.id,
        contentRecommend,
        id
      )
    })
}

getPostByID()
